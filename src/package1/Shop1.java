package package1;

import java.util.Objects;

public class Shop1  {
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}


    String name ;

    String Str;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Shop1)) return false;
        Shop1 shop1 = (Shop1) o;
        return name.equals(shop1.name) &&
                Str.equals(shop1.Str);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, Str);
    }

    @Override
    public String toString() {
        return "package1.Shop1 " +
                "name =  '" +name + "\'";
    }

    public static void main (String[] args){
        float[] price = new float[] {27.45f,756.34f,77.77f };
        System.out.println(price[1]);
        Shop1 s1 = new Shop1();
        s1.setName("Equator");
        Shop1 s2 = new Shop1();
        s2.setName("Eldorado");

        System.out.println("s1 =" +s1 );
        System.out.println(s1.equals(s2));
        System.out.println("s1.hashCode() = " + s1.hashCode());
        System.out.println("s2.hashCode() = " + s2.hashCode());







}
}
