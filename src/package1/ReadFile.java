package package1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
    public static void main(String[] args) {
        try {
        File file = new File("DEMO.Manager.txt");
        Scanner sc  = new Scanner(file);

        while (sc.hasNextLine())

            System.out.println(sc.nextLine());

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }


    }
}
