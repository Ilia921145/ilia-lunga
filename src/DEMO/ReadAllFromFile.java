package DEMO;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ReadAllFromFile {
    public static final String fileName = "file.txt";
    public static void ReadAllFromFile1 () {

        List<Manager> list = new ArrayList<>();

        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fr);
            String line = null;
            int currentLine = 0;


            while ((line = bufferedReader.readLine()) != null) {

                char delimiter = '|';
                int positionOfFirstDelimiter = line.indexOf(delimiter);
                int positionOfSecondDelimiter = line.indexOf(delimiter, positionOfFirstDelimiter + 1);
                int positionOfThirdDelimiter = line.indexOf(delimiter, positionOfSecondDelimiter + 1);
                int positionOfFourthDelimiter = line.indexOf(delimiter, positionOfThirdDelimiter + 1);
                int positionOfFifthDelimiter = line.indexOf(delimiter, positionOfFourthDelimiter + 1);

                String id = getId(line, positionOfFirstDelimiter + 1, positionOfSecondDelimiter);
                String name = getName(line, positionOfSecondDelimiter + 1, positionOfThirdDelimiter);
                String account = getAccount(line, positionOfThirdDelimiter + 1, positionOfFourthDelimiter);
                String email = getEmail(line, positionOfFourthDelimiter + 1, positionOfFifthDelimiter);

                Manager p1 = new Manager(id, name, account,email);

                p1.setId(id);
                p1.setName(name);
                p1.setAccount(account);
                p1.setEmail(email);

                list.add(p1);
                System.out.println(p1.toString());

            }
            bufferedReader.close();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String getId(String line, int positionOfFirstDelimiter, int positionOfSecondDelimiter){
        return line.substring(positionOfFirstDelimiter,positionOfSecondDelimiter);
    }

    private static String getName(String line, int positionOfSecondDelimiter, int positionOfThirdDelimiter){
        return line.substring(positionOfSecondDelimiter,positionOfThirdDelimiter);
    }

    private static String getAccount(String line, int positionOfThirdDelimiter, int positionOfFourthDelimiter){
        return line.substring(positionOfThirdDelimiter,positionOfFourthDelimiter);
    }

    private static String getEmail(String line, int positionOfFourthDelimiter, int positionOfFifthDelimiter){
        return line.substring(positionOfFourthDelimiter,positionOfFifthDelimiter);
    }

}














/*
import java.io.*;



public class FileWrite {





    public static void main(String[] args) {

        Manager m1 = new Manager(1, "Oleg", "Oleg1996", "Oleg1997@gmail.com");
        Manager m2 = new Manager(2, "Stephan", "Stephan1995", "Stephan1995@gmail.com");
        Manager m3 = new Manager(3, "Vasyl", "Vasyl1994", "Vasyl1994@gmail.com");
        Manager m4 = new Manager(4, "Liza", "Liza1993", "Liza1993@gmail.com");

            try {


                File file = new File("DEMO.Manager.txt");
                PrintWriter pw = new PrintWriter(file);

               */
/*  if ( m1.id < 2 )
                  m1.id = 0 ;*//*




                pw.println(m1);
                pw.println(m2);
                pw.println(m3);
                pw.println(m4);
                pw.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


*/
