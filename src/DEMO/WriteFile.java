package DEMO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class WriteFile {




    public static void String1 (){


        List<Manager> str = new ArrayList<>();
        Manager m1 = new Manager("|i1|", "Oleg|", "Oleg1994|","Oleg1994@gmail.com|");
        Manager m2 = new Manager("|i2|", "Vasyl|", "Vasyl1997|", "Vasyl1997@gmail.com|");
        Manager m3 = new Manager("|i3|", "Zina|", "Zina1993|", "Zina1993@gmail.com|");
        Manager m4 = new Manager("|i4|", "Lena|", "Lena1996|", "Lena1996@gmail.com|");


        str.add(m1);
        str.add(m2);
        str.add(m3);
        str.add(m4);



        Writer writer = null;
        try {
            writer = new FileWriter("file.txt");
            for (Manager line : str) {
                writer.write(String.valueOf(line));

                writer.write(System.getProperty("line.separator"));
            }
            writer.flush();

            } catch (Exception e) {
            Logger.getLogger(WriteFile.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                }
            }
        }


    }
}
